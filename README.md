# GeoJSON in GitLab

A very nice tool for creating your own `.geojson` file is [geojson.io](https://geojson.io). 

## 📚 References

- 📝 Docs: [GeoJSON files](https://docs.gitlab.com/ee/user/project/repository/geojson.html)
- 🚀 MR: [Add GeoJSON support for viewing files](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/123012)

### 🌐 Around the Internet

- 😎 [awesome-geojson](https://github.com/tmcw/awesome-geojson)
- 🛠️ [GeoJSON Utilities](https://jasonheppler.org/courses/csu-workshop/geojson-utilities.html)
- 🤓 [A primer on GeoJSON standard and visualization tools](https://sumit-arora.medium.com/what-is-geojson-geojson-basics-visualize-geojson-open-geojson-using-qgis-open-geojson-3432039e336d)
